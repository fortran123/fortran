const request = require('supertest');
const should= require('should');



describe('begin',function () {
    const server = require('../server')
    this.timeout(50000);
    const agent = request(server);
    it('get customers',(done)=>{

        agent.get('/newcus')
            .expect(200)
            .end(function (err,res) {
                if(err)
                {
                    return (err);
                }
                res.body.should.be.an.Array();
                done();
            });

    });

    it('get suppliers',(done)=>{

        agent.get('/newsup')
            .expect(200)
            .end(function (err,res) {
                if(err)
                {
                    return (err);
                }
                res.body.should.be.an.Array();
                done();
            });

    });

    it('get quality',(done)=>{

        agent.get('/po')
            .expect(200)
            .end(function (err,res) {
                if(err)
                {
                    return (err);
                }
                res.body.should.be.an.Array();
                done();
            });

    });


})
