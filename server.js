'use strict';

const bodyParser = require('body-parser');
const mongoose= require("mongoose");
const express= require("express");

mongoose.Promise = global.Promise;

require('./public/modules/supplier/user.model.js');
const userRouter = require('./public/modules/supplier/user.route');

require('./public/modules/supplier/supplier.model.js');
const supplierRouter = require('./public/modules/supplier/supplier.route');

require('./public/modules/supplier/supplyitem.model.js');
require('./public/modules/supplier/supplyorder.model.js');
const supplyitemRouter = require('./public/modules/supplier/supplyitem.route');

require('./public/modules/quality/goodreceived.model.js');
const poRouter = require('./public/modules/quality/po.routes');

require('./public/modules/sales/sales.model.js');
const salesRouter = require('./public/modules/sales/sales.route');

require('./public/modules/sales/salesDetails.model.js');
const salesDetailsRouter = require('./public/modules/sales/salesDetails.route');

require('./public/modules/common/notification.model.js');
const common = require('./public/modules/common/notification.route.js');

require('./public/modules/order1/order.model.js');
const ordersRouter = require('./public/modules/order1/order.route');

require('./public/modules/patient/customer.model');
const customerRouter = require('./public/modules/patient/customer.route');

require('./public/modules/stock/stockin.model.js');
const stockRouter = require('./public/modules/stock/stockin.route');

require('./public/modules/stock/stockout.model.js');
const stockOutRouter = require('./public/modules/stock/stockout.route');

require('./public/modules/stock/stockcount.model.js');
const stockCountRouter = require('./public/modules/stock/stockcount.route');

require('./public/modules/supplier/stockorder.model.js');
const stockorderRouter = require('./public/modules/supplier/stockorder.route');

const supplieremail = require('./public/modules/supplier/supplieremail.route');
const supplierinvoice = require('./public/modules/supplier/supplierinvoice.route');

const app = express();

app.use(bodyParser.json());

mongoose.connect('mongodb://root:123@ds019846.mlab.com:19846/fortran', err => {
    if (err) {
        console.log(err);
        process.exit(1);
    }
    else
    {
        console.log("connected");
    }
});


app.use('/newcus', userRouter);
app.use('/newsup', supplierRouter);
app.use('/sale',salesRouter);
app.use('/order',ordersRouter);
app.use('/customer', customerRouter);
app.use('/supplyitem',supplyitemRouter);
app.use('/salesDetails',salesDetailsRouter)
app.use('/common',common);
app.use('/stock', stockRouter);
app.use('/stockout', stockOutRouter);
app.use('/stockcount', stockCountRouter);
app.use('/po',poRouter);
app.use('/stockorder',stockorderRouter);
app.use('/supplieremail',supplieremail);
app.use('/supplierinvoice',supplierinvoice);

app.use(express.static('public'));

app.get('/', (req,res,next) => {
    res.sendFile(__dirname +'/pharmacy.html');
});


app.listen(3000,function(){
    console.log("con");
})

module.exports = app;