angular.module("myApp").controller('qualitycontroller',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {

    $http({
        method: 'GET',
        url: 'http://localhost:3000/po'
    }).then(function (success){
        $scope.po_grnnotRec=success.data;
        console.log($scope.po_grnnotRec)
    },function (error){
        console.log(error)
    });
    $http({
        method: 'GET',
        url: 'http://localhost:3000/po/all_po'
    }).then(function (success){
        $scope.all_po=success.data;
        console.log($scope.all_po)
    },function (error){
        console.log(error)
    });
    $scope.getagent=function (p) {


        $http({
            method: 'GET',
            url: 'http://localhost:3000/po/supplier/' + p.selectedName
        }).then(function (success) {
            $scope.ag_po = success.data;
            console.log($scope.ag_po)
        }, function (error) {
            console.log(error)
        });
    }

    $scope.getItemsFrom_specific_po_ID=function (po_ID) {

        $http({
            method: 'GET',
            url: 'http://localhost:3000/po/item/'+po_ID
        }).then(function (success){
            console.log('loch');
            $scope.po_ITEMS=success.data;
            console.log($scope.po_ITEMS)
        },function (error){
            console.log(error)
        });
    }
    $scope.locationChange=function () {
        $location.path('/po_ITEM_Checking');
    };

        $http({
            method: 'GET',
            url: 'http://localhost:3000/po/'+$routeParams.Po_ID+'/item/'+$routeParams.id
        }).then(function (success){
            console.log('loch');
            $scope.po_ITEMS=success.data[0];
            console.log($scope.po_ITEMS)
        },function (error){
            console.log(error)
        });

$scope.qualityChecking=function (getall,getall2) {
  var date1=new Date(getall.expiredate);
    var date2=new Date(getall2.expiredate);

    if (getall2.itemname == getall.itemname && getall2.itemquantity/2 <= getall.itemquantity && date1 >= date2  )
    {
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/po/'+$routeParams.Po_ID+'/item/'+$routeParams.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'quality': 'accepted',
                "status" :"checked"
            }
        }).then(function (success) {
            $scope.supplier = success.data[0];
            // add to the stock table
            $location.path('/po_GRNnotReceived');
        }, function (error) {
            console.log(error);
        });

        $http({
            method: 'POST',
            url: 'http://localhost:3000/po/goodrecieve',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'ordernumberitem':$routeParams.id ,
                'orderdetailnumber':$routeParams.Po_ID,
                'itemname': getall.itemname,
                'itemquantity': getall.itemquantity,
                'expiredate': getall.expiredate,
                'brand': getall.brand
            }
        }).then(function (success) {
            $scope.goodrecive = success.data;
        }, function (error) {
            console.log(error);
        });
    }
    else {
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/po/'+$routeParams.Po_ID+'/item/'+$routeParams.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'quality': 'rejected',
                "status" :"checked"
            }
        }).then(function (success) {
            $scope.supplier = success.data[0];
            // return show msg
            $location.path('/po_GRNnotReceived');
        }, function (error) {
            console.log(error);
        });
    }

}

$scope.updatepurchaseorder=function (purchaseorderid) {
    $http({
        method: 'PUT',
        url: 'http://localhost:3000/po/'+purchaseorderid,
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            'grnstatus': 'created',
           // 'qualitystatus':'accepted'
        }
    }).then(function (success) {
        $scope.supplier = success.data[0];
    }, function (error) {
        console.log(error);
    });
};


    $scope.updatepurchaseorder1=function (purchaseorderid) {
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/po/'+purchaseorderid,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'grnstatus': 'received',
                // 'qualitystatus':'accepted'
            }
        }).then(function (success) {
            $scope.supplier = success.data[0];
        }, function (error) {
            console.log(error);
        });
    };

    $http({
        method: 'GET',
        url: 'http://localhost:3000/po/rejected'
    }).then(function (success){
        $scope.rejecteditems=success.data;
        console.log($scope.rejecteditems)
    },function (error){
        console.log(error)
    });


    $scope.grnqty=function (po,i_ID) {

        $http({
            method: 'GET',
            url: 'http://localhost:3000/po/goodrecieve/' + po + '/qty/' +i_ID
        }).then(function (success) {
            $scope.grnitems = success.data;
            console.log($scope.grnitems)
        }, function (error) {
            console.log(error)
        });
    }
}]);
