angular.module("myApp").controller('orderaddcontroller',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {

    $scope.insertorderdata=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/order',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'orderId': $scope.orderId,
                'orderDes': $scope.orderDes,
                'quantity': $scope. quantity,
                'supplierId': $scope.supplierId,
                'GRN_status': $scope.GRN_status,
                'quality_status': $scope.quality_status
            }
        }).then(function (success) {
            $scope.order = success.data;
            $scope.getordersucessmsg ='Successfully';
            $scope.showAlert();
           // $scope.getall();
            $http({
                method: 'GET',
                url: 'http://localhost:3000/order'
            }).then(function (success) {
                $scope.orderdata = success.data;
            }, function (error) {

            });
        }, function (error) {
            $scope.getordersucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
            //$scope.getall();
            $http({
                method: 'GET',
                url: 'http://localhost:3000/order'
            }).then(function (success) {
                $scope.orderdata = success.data;
            }, function (error) {

            });

        });
    };

    $scope.showAlert = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        console.log($scope.getordersucessmsg);
        if($scope.getordersucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getordersucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };
    $scope.showAlert_1 = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        console.log($scope.getordersucessmsg_1);
        if($scope.getordersucessmsg_1=="Quality is good ,Add Goods details to stock")
            $scope.mytrue = true;
        else if($scope.getordersucessmsg_1=="Quality is not good , Stock Return")
            $scope.myfalse = true;
    };

    $scope.getall=function () {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/order'
        }).then(function (success) {
            $scope.orderdata = success.data;
        }, function (error) {

        });
    };

    $http({
        method: 'GET',
        url: 'http://localhost:3000/order/'+$routeParams.id
    }).then(function (success) {
        $scope.orderdataspecific = success.data[0];
    }, function (error) {

    });

    $scope.updatedata=function () {
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/order/'+$scope.orderdataspecific.orderId,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'orderDes': $scope.orderdataspecific.orderDes,
                'quantity': $scope.orderdataspecific.quantity,
                'supplierId': $scope.orderdataspecific.supplierId,
                'GRN_status': $scope.orderdataspecific.GRN_status,
                'quality_status': $scope.orderdataspecific.quality_status

            }
        }).then(function (success) {
            $scope.order = success.data[0];
            $scope.getordersucessmsg ='Successfully';
            $scope.showAlert();
          console.log($scope.orderdataspecific.quality_status);
            if($scope.orderdataspecific.quality_status=== "good")
            {
                $scope.getordersucessmsg_1 ='Quality is good ,Add Goods details to stock';
                $scope.showAlert_1();
            }
            else if($scope.orderdataspecific.quality_status=== "damaged")
                {
                console.log("return");
                $scope.getordersucessmsg_1 ='Quality is not good , Stock Return';
                $scope.showAlert_1();
            }
            else {
                console.log("not checked");

            }

        }, function (error) {
            $scope.getordersucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };
    $scope.quality_names = ["not checked", "good", "damaged"];
    $scope.grn_names = ["not received", "received"];
    $scope.orderDes_names = ["priton", "panadol","motilum","meneril","vitamin C"];
    $scope.supplierId_names = ["S_1", "S_2","S_3", "S_4","S_5", "S_6"];

    $scope.options = [{ name: "a", id: 1 }, { name: "b", id: 2 }];
    $scope.selectedOption = $scope.options[1];


    $scope.deletedata=function () {
        $http({
            method: 'DELETE',
            url: 'http://localhost:3000/order/'+$routeParams.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'orderId': $scope.orderId,
                'orderDes': $scope.orderDes,
                'quantity': $scope. quantity,
                'supplierId': $scope.supplierId,
                'GRN_status': $scope.GRN_status,
                'quality_status': $scope.quality_status
            }
        }).then(function (success) {
            $scope.order = success.data[0];
            $scope.getordersucessmsg ='Successfully';
            $scope.showAlert();
            $location.path('/orderview');
        }, function (error) {
            $scope.getordersucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };
    $http({
        method: 'GET',
        url: 'http://localhost:3000/order'
    }).then(function (success) {
        $scope.orderdata = success.data;
    }, function (error) {

    });

}]);
