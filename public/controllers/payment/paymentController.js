angular.module("myApp").controller('paymentController',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {


    $scope.insertpaymentdata=function () {

        $http({
            method: 'POST',
            url: 'http://localhost:8080/payment',
            headers: {
                'Content-Type': 'application/json'
            },
             data: {
            "cardNumber": $scope.cardNumber,
            "createdAt": $scope.date,
            "customerId": $scope.customerRequirement,
            "grossTotal":$scope.netTotal,
            "invoiceId": $scope.docCount +1,
            "paymentID": "1",
            "status": "1",
            "type": $scope.paymentRequirement
        }

        }).then(function (success) {
            $scope.paymentdata = success.data;
            $scope.getpaymentsucessmsg ='Successfully';
            $scope.showAlert();
        }, function (error) {
            $scope.getpaymentsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };

    $scope.showAlert = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        console.log($scope.getpaymentsucessmsg);
        if($scope.getpaymentsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getpaymentsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };

/////////////////////////get products from stock///////////////////
    $http({
        method: 'GET',
        url: 'http://localhost:8080/stock'
    }).then(function (success) {
        $scope.productData = success.data;
        console.log($scope.productData);
    }, function (error) {

    });

    /////////////////////////get customers from customer///////////////////
    $http({
        method: 'GET',
        url: 'http://localhost:3000/customer'
    }).then(function (success) {
        $scope.customerData = success.data;
        console.log($scope.customerData);
    }, function (error) {

    });
// /////////////////////// send to cart////////////////////////////////

    $scope.cartItems = [
            ];
    $scope.payed = [
    ];
    $scope.cartItemsforcheck = [
    ];
    $scope.netTotal = [
    ];
    $scope.grossTotalvalue=0.00;

    $scope.date = new Date();

////////////////////////////////////////////////////////

    $http({
        method: 'GET',
        url: 'http://localhost:8080/getcountpayment/'
    }).then(function (success) {
        $scope.docCount = success.data;
        console.log($scope.docCount +1);
    }, function (error) {

    });

    $scope.InvoiceIdCount=$scope.docCount +1;
    console.log($scope.InvoiceIdCount);

////////////////////////////////////////////////////////

    $scope.sendToCart=function (param) {

    $scope.cartItems.push({
        'productName': param.param.productName,
        'unitAmount': param.param.unitAmount

    });
    };

    /////////////////////////get Net total //////////////

    $scope.total = function() {
        console.log(",,,,,,,");
        var total = 0;
        for (var i=0; i<$scope.cartItemsforcheck.length; i++) {
            total +=$scope.cartItemsforcheck[i].total ;
            console.log($scope.cartItemsforcheck[i].total);
        }
        $scope.netTotal=total;
        return total;
    }

//////////////////////////////////////////

    $scope.sendTocheckout=function (param1) {

        $scope.cartItemsforcheck.push({
            'productName': param1.param1.productName,
            'unitAmount': param1.param1.unitAmount,
            'qty': param1.param1.qty,
            'total': param1.param1.unitAmount*param1.param1.qty

        });
        console.log($scope.cartItemsforcheck);
    };


    ////////////////////pay (check Out)///////////////////////////////

    $scope.pay=function () {
        console.log("inside");
        $scope.payed.push({
            'productlist': {ids:$scope.cartItemsforcheck},
            'invoiceId': '002',
            "customerId": $scope.customerRequirement,
            "grossTotal":$scope.netTotal,
            "type": $scope.paymentRequirement,
            "createdAt": "2017-07-01"
        });
        console.log($scope.payed);
    };



    ///////////////////////add sales details///////////

    $scope.insertsalesdata=function (i) {
        $http({
            method: 'POST',
            url: 'http://localhost:8080/salesDetails',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'salesDetailsId':'1',
                'invoiceId':  $scope.docCount +1,
                'productName': $scope.cartItemsforcheck[i].productName,
                'unitAmount': $scope.cartItemsforcheck[i].unitAmount,
                'qty': $scope.cartItemsforcheck[i].qty,
                'total': $scope.cartItemsforcheck[i].total,
                "createdAt": $scope.date
            }

        }).then(function (success) {
            $scope.paymentdata = success.data;
            $scope.getpaymentsucessmsg ='Successfully';
            $scope.showAlert();
        }, function (error) {
            $scope.getpaymentsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };



    $scope.insertsalesdataloop = function() {

        for (var i=0; i<$scope.cartItemsforcheck.length; i++) {
            $scope.insertsalesdata(i);
        }

    };


    ///////////////////////////////////////////

    $scope.locationchange=function () {
        console.log($scope.payed+"  location changed");
        $location.path('/salesDetails/'+($scope.docCount +1));
    };
/////////////////////////////////////////////////////////





   }]);

