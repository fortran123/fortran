angular.module("myApp").controller('stockOutController',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {


    $scope.insertstockoutdata=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/stockout',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'ProductID': $scope.ProductID,
                'PackageID': $scope.PackageID,
                'Brand': $scope.Brand,
                'DrugName': $scope.DrugName,
                'Quantity': $scope.Quantity,
                'LotNo':$scope.LotNo,
                'NameOfManufacturerer':$scope.NameOfManufacturerer,
                'StockOutDate':$scope.StockOutDate,
                'Reason':$scope.Reason
            }
        }).then(function (success) {
            $scope.stockout = success.data;
            $scope.getstockoutsucessmsg ='Successfully';
            $scope.showAlert1();
        }, function (error) {
            $scope.getstockoutsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert1();
        });
    };

    $scope.showAlert1 = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        console.log($scope.getstockoutsucessmsg);
        if($scope.getstockoutsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getstockoutsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };




    $http({
        method: 'GET',
        url: 'http://localhost:3000/stockout',
    }).then(function (success){
        $scope.stockoutdata=success.data;
        console.log($scope.stockoutdata);
    },function (error){

    });

    $http({
        method: 'GET',
        url: 'http://localhost:3000/stockout/'+$routeParams.id,
    }).then(function (success) {
        $scope.stockindataspecific = success.data[0];
    }, function (error) {

    });

    $scope.updatestockout=function () {
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/stockout/'+$scope.stockoutdataspecific.ProductID,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'ProductID': $scope.stockoutdataspecific.ProductID,
                'PackageID': $scope.stockoutdataspecific.PackageID,
                'Brand': $scope.stockoutdataspecific.Brand,
                'DrugName': $scope.stockoutdataspecific.DrugName,
                'Quantity':$scope.stockoutdataspecific.Quantity,
                'LotNo':$scope.stockoutdataspecific.LotNo,
                'NameOfManufacturerer':$scope.stockoutdataspecific.NameOfManufacturerer,
                'StockOutDate':$scope.stockoutdataspecific.StockOutDate,
                'Reason':$scope.stockoutdataspecific.StorageRequirement,
                'updatedAt':Date.now()
            }
        }).then(function (success) {
            $scope.stockout = success.data;
            $scope.getstockoutsucessmsg ='Successfully';
            $scope.showAlert1();

        }, function (error) {
            $scope.getstockoutsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert1();
        });
    };


    $scope.deleteout=function (id) {
        $scope.passid=id;
        $http({
            method: 'DELETE',
            url: 'http://localhost:3000/stockout/'+$scope.passid,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'ProductID': $scope.ProductID,
                'PackageID': $scope.PackageID,
                'Brand': $scope.Brand,
                'DrugName': $scope.DrugName,
                'Quantity': $scope.Quantity,
                'LotNo':$scope.LotNo,
                'NameOfManufacturerer':$scope.NameOfManufacturerer,
                'StockOutDate':$scope.StockOutDate,
                'Reason':$scope.Reason
            }
        }).then(function (success) {
            $scope.stockout = success.data[0];
                $http({
                    method: 'GET',
                    url: 'http://localhost:3000/stockout',
                }).then(function (success){
                    $scope.stockoutdata=success.data;
                    console.log($scope.stockoutdata);
                },function (error){

                });
            $location.path('/stockoutview');
        }, function (error) {
            console.log(error)
        });
    };

    $scope.reloadtable=function () {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/stockout',
        }).then(function (success) {
            $scope.stockoutdata = success.data;
        }, function (error) {

        });
    };
}]);

