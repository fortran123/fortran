angular.module("myApp").controller('stockInController',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {


    $scope.insertstockindata=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/stock',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'ProductID': $scope.ProductID,
                'PackageID': $scope.PackageID,
                'Brand': $scope.Brand,
                'DrugName': $scope.DrugName,
                'DosageForm': $scope.DosageForm,
                'Quantity': $scope.Quantity,
                'NationalDrugCode':$scope.NationalDrugCode,
                'LotNo':$scope.LotNo,
                'ExpirationDate':$scope.ExpirationDate,
                'NameOfManufacturerer':$scope.NameOfManufacturerer,
                'StorageRequirement':$scope.StorageRequirement
            }
        }).then(function (success) {
            $scope.stockin = success.data;
            $scope.getstockinsucessmsg ='Successfully';
            $scope.showAlert1();
        }, function (error) {
            $scope.getstockinsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert1();
        });
    };

    $scope.showAlert1 = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        console.log($scope.getstockinsucessmsg);
        if($scope.getstockinsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getstockinsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };




    $http({
        method: 'GET',
        url: 'http://localhost:3000/stock',
    }).then(function (success){
        $scope.stockindata=success.data;
    },function (error){

    });

    $http({
        method: 'GET',
        url: 'http://localhost:3000/stock/'+$routeParams.id,
    }).then(function (success) {
        $scope.stockindataspecific = success.data[0];
    }, function (error) {

    });

    $scope.updatestockin=function () {
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/stock/'+$scope.stockindataspecific.ProductID,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'ProductID': $scope.stockindataspecific.ProductID,
                'PackageID': $scope.stockindataspecific.PackageID,
                'Brand': $scope.stockindataspecific.Brand,
                'DrugName': $scope.stockindataspecific.DrugName,
                'DosageForm':$scope.stockindataspecific.DosageForm,
                'Quantity':$scope.stockindataspecific.Quantity,
                'NationalDrugCode':$scope.stockindataspecific.NationalDrugCode,
                'LotNo':$scope.stockindataspecific.LotNo,
                'ExpirationDate':$scope.stockindataspecific.ExpirationDate,
                'NameOfManufacturerer':$scope.stockindataspecific.NameOfManufacturerer,
                'StorageRequirement':$scope.stockindataspecific.StorageRequirement,
                'updatedAt':Date.now()
            }
        }).then(function (success) {
            $scope.stockin = success.data;
            $scope.getstockinsucessmsg ='Successfully';
            $scope.showAlert1();

        }, function (error) {
            $scope.getstockinsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert1();
        });
    };


    $scope.deletedata=function (id) {
        $scope.passid=id;
        $http({
            method: 'DELETE',
            url: 'http://localhost:3000/stock/'+$scope.passid,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'ProductID': $scope.ProductID,
                'PackageID': $scope.PackageID,
                'Brand': $scope.Brand,
                'DrugName': $scope.DrugName,
                'DosageForm': $scope.DosageForm,
                'Quantity': $scope.Quantity,
                'NationalDrugCode':$scope.NationalDrugCode,
                'LotNo':$scope.LotNo,
                'ExpirationDate':$scope.ExpirationDate,
                'NameOfManufacturerer':$scope.NameOfManufacturerer,
                'StorageRequirement':$scope.StorageRequirement
            }
        }).then(function (success) {
            $scope.stockin = success.data[0];
            //console.log($routeParams.id+"okkkkkkkkkkkkkkkkkkkkkk")
            $http({
                method: 'GET',
                url: 'http://localhost:3000/stock',
            }).then(function (success){
                $scope.stockindata=success.data;
                console.log($scope.stockindata);
            },function (error){

            });
            $location.path('/stockinview');
        }, function (error) {
            console.log(error)
        });
    };

    $scope.reloadtable=function () {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/stock',
        }).then(function (success) {
            $scope.stockindata = success.data;
        }, function (error) {

        });
    };
}]);
