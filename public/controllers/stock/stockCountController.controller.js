angular.module("myApp").controller('stockCountController',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {


    $scope.insertstockcountdata=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/stockcount',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'PackageID': $scope.PackageID,
                'Brand': $scope.Brand,
                'DrugName': $scope.DrugName,
                'QuantityOfProductsOrdered':$scope.QuantityOfProductsOrdered,
                'RemainingQuantity': $scope.RemainingQuantity,
                'LotNo':$scope.LotNo,
                'ExpirationDate':$scope.ExpirationDate,
                'StockCountDate':$scope.StockCountDate,
                'StockCountMethod':$scope.StockCountMethod,
                'NameOfManufacturerer':$scope.NameOfManufacturerer
            }
        }).then(function (success) {
            $scope.stockcount = success.data;
            $scope.getstockcountsucessmsg ='Successfully';
            $scope.showAlert1();
        }, function (error) {
            $scope.getstockcountsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert1();
        });
    };

    $scope.showAlert1 = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        console.log($scope.getstockcountsucessmsg);
        if($scope.getstockcountsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getstockcountsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };




    $http({
        method: 'GET',
        url: 'http://localhost:3000/stockcount',
    }).then(function (success){
        $scope.stockcountdata=success.data;
    },function (error){

    });

    $http({
        method: 'GET',
        url: 'http://localhost:3000/stockcount/'+$routeParams.id,
    }).then(function (success) {
        $scope.stockcountdataspecific = success.data[0];
    }, function (error) {

    });

    $scope.updatestockcount=function () {
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/stockcount/'+$scope.stockcountdataspecific.ProductID,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'PackageID': $scope.stockcountdataspecific.PackageID,
                'Brand': $scope.stockcountdataspecific.Brand,
                'DrugName': $scope.stockcountdataspecific.DrugName,
                'QuantityOfProductsOrdered':$scope.stockcountdataspecific.QuantityOfProductsOrdered,
                'RemainingQuantity': $scope.stockcountdataspecific.RemainingQuantity,
                'LotNo':$scope.stockcountdataspecific.LotNo,
                'ExpirationDate':$scope.stockcountdataspecific.ExpirationDate,
                'StockCountDate':$scope.stockcountdataspecific.StockCountDate,
                'StockCountMethod':$scope.stockcountdataspecific.StockCountMethod,
                'NameOfManufacturerer':$scope.stockcountdataspecific.NameOfManufacturerer,
                'updatedAt':Date.now()
            }
        }).then(function (success) {
            $scope.stockcount = success.data;
            $scope.getstockcountsucessmsg ='Successfully';
            $scope.showAlert1();

        }, function (error) {
            $scope.getstockcountsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert1();
        });
    };


    $scope.deletedata=function () {
        $http({
            method: 'DELETE',
            url: 'http://localhost:3000/stockcount/'+$routeParams.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'PackageID': $scope.PackageID,
                'Brand': $scope.Brand,
                'DrugName': $scope.DrugName,
                'QuantityOfProductsOrdered':$scope.QuantityOfProductsOrdered,
                'RemainingQuantity': $scope.RemainingQuantity,
                'LotNo':$scope.LotNo,
                'ExpirationDate':$scope.ExpirationDate,
                'StockCountDate':$scope.StockCountDate,
                'StockCountMethod':$scope.StockCountMethod,
                'NameOfManufacturerer':$scope.NameOfManufacturerer
            }
        }).then(function (success) {
            $scope.stockcount= success.data[0];
            $scope.getstockcountsucessmsg ='Successfully';
            $scope.showAlert();
            $location.path('/stockcount');
        }, function (error) {
            $scope.getstockcountsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };

    $scope.reloadtable=function () {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/stockcount',
        }).then(function (success) {
            $scope.stockcountdata = success.data;
        }, function (error) {

        });
    };
}]);
