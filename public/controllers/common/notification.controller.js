angular.module("myApp").controller('notification',['$scope','$http',function ($scope,$http) {
    $http({
        method: 'GET',
        url: 'http://localhost:3000/common'
    }).then(function (success){
        $scope.commondata=success.data;
        $scope.noofnotifications=$scope.commondata.length;
    },function (error){

    });

    $scope.notifyupdate=function (id) {
        $scope.idno=id;
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/common',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'id': $scope.idno,
                'status': 'true'
            }
        }).then(function (success) {
            $scope.notify = success.data;
                //toreload
                $http({
                    method: 'GET',
                    url: 'http://localhost:3000/common'
                }).then(function (success){
                    $scope.commondata=success.data;
                    $scope.noofnotifications=$scope.commondata.length;
                },function (error){

                });
        }, function (error) {
            console.log(error);
        });
    };
    $scope.insertnotification=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/common',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'name': $scope.name,
                'message': $scope.message,
                'status': 'false'
            }
        }).then(function (success) {
            $scope.notify = success.data;
                //toreload
                $http({
                    method: 'GET',
                    url: 'http://localhost:3000/common'
                }).then(function (success){
                    $scope.commondata=success.data;
                    $scope.noofnotifications=$scope.commondata.length;
                },function (error){

                });
        }, function (error) {
            console.log(error);
        });
    };

}]);