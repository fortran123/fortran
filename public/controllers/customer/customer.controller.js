/**
 * Created by Loch on 6/30/2017.
 */
angular.module("myApp").controller('cuscontroller',['$scope','$http',function ($scope,$http) {

    $scope.showAlert = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        // console.log($scope.getsupsucessmsg);
        if($scope.getsupsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getsupsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };

    $scope.insertdata=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/customer',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {

                'CustomerName': $scope.CustomerName,
                'CustomerTelephone': $scope.CustomerTelephone,
                'CustomerSpecilization': $scope.CustomerSpecilization
            }
        }).then(function (success) {
            $scope.userdata = success.data;
            // console.log($scope.userdata);

            //$scope.order = success.data;
            $scope.getsupsucessmsg ='Successfully';
            $scope.showAlert();
            $scope.getall();

        }, function (error) {
            $scope.getsupsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
            $scope.getall();

        });
    };

}]);

