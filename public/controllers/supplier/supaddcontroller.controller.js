angular.module("myApp").controller('supaddcontroller',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {

    $scope.insertsupdata=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/newsup',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'companyname': $scope.companyname,
                'agentname': $scope.agentname,
                'address': $scope.address,
                'createddate': $scope.createddate,
                'period': $scope.period,
                'contactno': $scope.contactno,
                'category':$scope.category
            }
        }).then(function (success) {
            $scope.supplier = success.data;
            $scope.getsupsucessmsg ='Successfully';
            $scope.showAlert();
                $scope.companyname= null;
                $scope.agentname= null;
                $scope.address= null;
                $scope.createddate= null;
                $scope.period= null;
                $scope.contactno= null;
                $scope.category= null;
        }, function (error) {
            $scope.getsupsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };

    $scope.showAlert = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        console.log($scope.getsupsucessmsg);
        if($scope.getsupsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getsupsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };

    $http({
        method: 'GET',
        url: 'http://localhost:3000/newsup'
    }).then(function (success){
        $scope.supplierdata=success.data;
    },function (error){

    });

    $http({
        method: 'GET',
        url: 'http://localhost:3000/newsup/'+$routeParams.id
    }).then(function (success) {
        $scope.supplierdataspecific = success.data[0];
    }, function (error) {

    });

    $scope.updatedata=function (updatesupid) {
        $http({
            method: 'PUT',
            url: 'http://localhost:3000/newsup/'+updatesupid,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'companyname': $scope.supplierdataspecific.companyname,
                'agentname': $scope.supplierdataspecific.agentname,
                'address': $scope.supplierdataspecific.address,
                'createddate': $scope.supplierdataspecific.createddate,
                'period': $scope.supplierdataspecific.period,
                'contactno': $scope.supplierdataspecific.contactno,
                'category':$scope.supplierdataspecific.category
            }
        }).then(function (success) {
            $scope.supplier = success.data[0];
            $scope.getsupsucessmsg ='Successfully';
            $scope.showAlert();
        }, function (error) {
            $scope.getsupsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };

    $scope.deletedata=function (suppid) {
        $http({
            method: 'DELETE',
            url: 'http://localhost:3000/newsup/'+suppid,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (success) {
            $scope.supplierdelete = success.data;
            $scope.reloadtable();
        }, function (error) {
            $scope.console(error);
        });
    };

    $scope.reloadtable=function () {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/newsup'
        }).then(function (success) {
            $scope.supplierdata = success.data;
        }, function (error) {

        });
    };
    $scope.locationchange=function () {
        $location.path('/suppliersview');
    };
    $scope.locationchangehome=function () {
        $location.path('/');
    };
}]);
