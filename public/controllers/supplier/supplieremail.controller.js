angular.module("myApp").controller('suppliermailcontroller',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {

    $scope.sendmail=function (email) {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/supplieremail',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'email': $scope.email.supplieremail,
                'text': $scope.email.emailtext
            }
        }).then(function (success) {
            $scope.supplier = success.data;
            $scope.getsupsucessmsg ='Successfully';
            $scope.showAlert();
        }, function (error) {
            $scope.getsupsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };

    $scope.showAlert = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        console.log($scope.getsupsucessmsg);
        if($scope.getsupsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getsupsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };

}]);