angular.module("myApp").controller('supitemaddcontroller',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {

    //$scope.supplyitemdata=[];
    $scope.insertorderdata=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/supplyitem',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {

                'ordernumber': $scope.ordernumber,
                'selectedName': $scope.selectedName,
                'description': $scope.description,
                'createddate': $scope.createddate,
                'grnstatus': 'not received',
                'qualitystatus': 'not checked'
            }
        }).then(function (success) {
            $scope.supplierorder = success.data;
            $scope.getsupsucessmsg ='Successfully';
            $scope.showAlert();
            $location.path('/supplyorderadd/'+$scope.ordernumber);
        }, function (error) {
            console.log(error)
            $scope.getsupsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };

    $scope.insertitemdata=function (item) {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/supplyitem/item',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {

                'ordernumberitem': $scope.item.ordernumber,
                'orderdetailnumber': $scope.item.orderdetailnumber,
                'itemname': $scope.item.itemname,
                'itemquantity': $scope.item.itemquantity,
                'itemprice': $scope.item.itemprice,
                'expiredate':$scope.item.expiredate,
                'brand':$scope.item.brand,
                'status':'not check',
                'quality':'not check'
            }
        }).then(function (success) {
            $scope.supplieritem = success.data;
            $scope.getsupsucessmsg2 ='Successfully';
            $scope.showAlert();
            $scope.reloadorderitems();
            $scope.item.orderdetailnumber = null;
            $scope.item.itemname = null;
            $scope.item.itemquantity = null;
            $scope.item.expiredate = null;
            $scope.item.brand = null;
            $scope.item.itemprice = null;
        }, function (error) {
            console.log(error)
            $scope.getsupsucessmsg2 ='Something Went Wrong!!!!';
            $scope.showAlert();
        });
    };

    $scope.showAlert = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
        $scope.mytrue2 = false;
        $scope.myfalse2 = false;
        console.log($scope.getsupsucessmsg);
        if($scope.getsupsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getsupsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
        if($scope.getsupsucessmsg2=="Successfully")
            $scope.mytrue2 = true;
        else if($scope.getsupsucessmsg2=="Something Went Wrong!!!!")
            $scope.myfalse2 = true;
    };

    $http({
        method: 'GET',
        url: 'http://localhost:3000/supplyitem/item/'+$routeParams.id,
    }).then(function (success){
        $scope.supplyitemdata=success.data;
        console.log($scope.supplyitemdata)
    },function (error){
        console.log(error)
    });

    $http({
        method: 'GET',
        url: 'http://localhost:3000/supplyitem/'+$routeParams.id
    }).then(function (success){
        $scope.item=success.data[0];
        console.log($scope.item)
    },function (error){
        console.log(error)
    });

    $scope.locationchange=function () {
        $location.path('/');
    };
    $scope.locationchangehome=function () {
        $location.path('/');
    };

    $scope.reloadorderitems=function () {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/supplyitem/item/'+$routeParams.id,
        }).then(function (success){
            $scope.supplyitemdata=success.data;
            console.log($scope.supplyitemdata)
        },function (error){
            console.log(error)
        });
    };

    $scope.deleteitemdata=function (deleteid) {
        $http({
            method: 'DELETE',
            url: 'http://localhost:3000/supplyitem/item/'+deleteid,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (success) {
            $scope.itemdelete = success.data;
            $scope.reloadorderitems();
        }, function (error) {
            console.log(error)
        });
    };

    $http({
        method: 'GET',
        url: 'http://localhost:3000/newsup'
    }).then(function (success){
        $scope.supplyitemdata2=success.data;
    },function (error){

    });

}]);
