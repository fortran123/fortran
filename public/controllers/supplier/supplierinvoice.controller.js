angular.module("myApp").controller('supplierinvoice',['$scope','$http','$routeParams','$location',function ($scope,$http,$routeParams,$location) {

    $scope.date = new Date();

    $http({
        method: 'GET',
        url: 'http://localhost:3000/supplierinvoice'
    }).then(function (success){
        $scope.supplierinvoice=success.data;
    },function (error){

    });

    $scope.getspecificitems = function (invoiceid) {

        $http({
            method: 'GET',
            url: 'http://localhost:3000/supplierinvoice/'+invoiceid
        }).then(function (success){
            $scope.supplieditems=success.data;
            $scope.total = 0;
            for( $scope.i = 0; $scope.i < $scope.supplieditems.length; $scope.i++){
                $scope.product = $scope.supplieditems[$scope.i];
                $scope.total = parseInt($scope.total) + parseInt($scope.product.itemprice);
            }
        },function (error){
            console.log(error);
        });

        // $scope.getTotal = function(){
        //      $scope.total = 0;
        //     for( $scope.i = 0; $scope.i < $scope.supplieditems.length; $scope.i++){
        //         $scope.product = $scope.supplieditems[i];
        //         $scope.total = $scope.total + $scope.product.itemprice;
        //     }
        //     return $scope.total;
        // }
    }
}]);