angular.module("myApp").controller('empcontroller',['$scope','$http',function ($scope,$http) {
    $scope.getall=function () {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/newcus'
        }).then(function (success) {
            $scope.userdata = success.data;
        }, function (error) {

        });
    };
    $scope.getspecific=function () {


        $http({
            method: 'GET',
            url: 'http://localhost:3000/newcus/' + $routeParams.id
        }).then(function (success) {
            $scope.news = success.data[0];
        }, function (error) {

        });
    }
    $scope.showAlert = function(){
        $scope.mytrue = false;
        $scope.myfalse = false;
       // console.log($scope.getsupsucessmsg);
        if($scope.getsupsucessmsg=="Successfully")
            $scope.mytrue = true;
        else if($scope.getsupsucessmsg=="Something Went Wrong!!!!")
            $scope.myfalse = true;
    };
    $scope.diagnosis_names = ["pressure", "migrain", "Diabitics"];
    $scope.patient_type_names = ["in-ward", "out-ward"];
    $http({
        method: 'GET',
        url: 'http://localhost:3000/newcus'
    }).then(function (success) {
        $scope.userdata = success.data;
    }, function (error) {

    });

    $scope.insertdata=function () {
        $http({
            method: 'POST',
            url: 'http://localhost:3000/newcus',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {

                'firstname': $scope.firstname,
                'lastname': $scope.lastname,
                'cus_ID': $scope.cus_ID,

                'address': $scope.address,
                'date': $scope.date,
                'time': $scope.time,

                'diagnosis': $scope.diagnosis,
                'contactno': $scope.contactno,
                'patient_type': $scope.patient_type,



            }
        }).then(function (success) {
            $scope.userdata = success.data;
           // console.log($scope.userdata);

            //$scope.order = success.data;
            $scope.getsupsucessmsg ='Successfully';
            $scope.showAlert();
            $scope.getall();

        }, function (error) {
            $scope.getsupsucessmsg ='Something Went Wrong!!!!';
            $scope.showAlert();
            $scope.getall();

        });
    };

}]);

