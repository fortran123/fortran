angular.module("myApp").config(function($routeProvider) {
    $routeProvider
        .when("/red", {
            templateUrl : "views/supplier/tables_dynamic.html"
        })
        .when("/blue", {
            templateUrl : "views/patient/table.html"
        })
        .when("/userAdd", {
            templateUrl : "views/patient/userAdd.html"
        })
        .when("/po_GRNnotReceived", {
            templateUrl : "views/quality_managment/po_select.html"
        })
        .when("/po_ITEM_Checking/:Po_ID/:id/", {
            templateUrl : "views/quality_managment/po_specific.html"
        })
        .when("/suppliersview/:id", {
            templateUrl : "views/supplier/updatesupplier.html"
        })
        .when("/suppliersview", {
            templateUrl : "views/supplier/suppliersview.html"
        })
        .when("/supplieradd", {
            templateUrl : "views/supplier/supplieradd.html"
        })
        .when("/supplyorderadd", {
            templateUrl : "views/supplier/supplyorderadd.html"
        })
        .when("/supplyorderadd/:id", {
            templateUrl : "views/supplier/supplyitemadd.html"
        })
        .when("/invoice", {
            templateUrl : "views/sales/invoice.html"
        })
        .when("/sales", {
            templateUrl : "views/sales/salesView.html"
        })
        .when("/sales/:id", {
            templateUrl : "views/sales/salesViewUpdate.html"
        })
        .when("/salesDetails/:param", {
            templateUrl : "views/sales/salesDetail.html"
        })
        .when("/newreturnsales", {
            templateUrl : "views/sales/returnSalesAdd.html"
        })
        .when("/returnsales", {
            templateUrl : "views/sales/returnSalesView.html"
        })
        .when("/salesreports", {
            templateUrl : "views/sales/salesReport.html"
        })
        .when("/orderview", {
            templateUrl : "views/order1/orderview.html"
        })
        .when("/ordersview/:id", {
            templateUrl : "views/order1/updateorder.html"
        })
        .when("/doctor", {
            templateUrl : "views/customer/customerAdd.html"
        })
        .when("/orderadd", {
            templateUrl : "views/order1/orderadd.html"
        })
        .when("/stockin", {
            templateUrl : "views/stock/stockin.html"
        })
        .when("/stockinview", {
            templateUrl : "views/stock/stockinview.html"
        })
        .when("/stockout", {
            templateUrl : "views/stock/stockout.html"
        })
        .when("/stockoutview", {
            templateUrl : "views/stock/stockoutview.html"
        })
        .when("/stockcount", {
            templateUrl : "views/stock/stockcount.html"
        })

        .when("/stockorder", {
            templateUrl : "views/supplier/stockorder.html"
        })
        .when("/stockorder/:id", {
            templateUrl : "views/supplier/stockorderitem.html"
        })
        .when("/supplieremail", {
            templateUrl : "views/supplier/supplieremail.html"
        })
        .when("/stockcount", {
            templateUrl : "views/stock/stockcount.html"
        })
        .when("/rejected", {
            templateUrl : "views/quality_managment/rejecteditems.html"
        })
        .when("/supplierinvoice", {
            templateUrl : "views/supplier/supplierinvoice.html"
        })
        .when("/grnreport", {
            templateUrl : "views/quality_managment/invoice.html"
        })
        .otherwise({
            templateUrl : "views/supplier/form.html"
        });
});