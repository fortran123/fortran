const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const orderModel = mongoose.model('order');
const Router = express.Router();



Router.post('/', (req,res) => {
    const order = new orderModel(req.body);
    order.save().then(order => {
        res.json(order);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/', (req, res) => {
    orderModel.find().exec().then(orderdata => {
        res.json(orderdata);
        //console.log(req);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/:id', (req, res) => {

   // console.log("fff")
    orderModel.find({ "orderId": req.params.id }).exec().then(orderdataspecific => {
        res.json(orderdataspecific);
    //console.log(req);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});


Router.put('/:id', (req, res) => {
    const details = req.body;

    //console.log(req.body);
    delete details. orderId;
    const  orderIda = req.params.id;
    orderModel.update({ orderId: orderIda}, {$set: details}).then(orderupdate => {
        console.log("dsadasdasdasdasd");
        console.log(req.body);
        console.log( orderIda);
        res.json(orderupdate);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

//////////////////////


// Router.put('/:id', (req, res) => {
//     const details = req.body;
//     delete details.companyname;
//     const customernameparam = req.params.id;
//     salesModel.update({companyname:customernameparam}, {$set: details}).then(salesupdate => {
//         console.log(req.body);
//         res.json(salesupdate);
//     }).catch(err => {
//         console.error(err);
//         res.sendStatus(500);
//     });
// });


Router.delete('/:id', (req, res) => {
    const details = req.body;
    delete details. orderId;
    const  orderIda = req.params.id;
    orderModel.remove({ orderId: orderIda}).then(orderdelete => {
        res.sendStatus(200);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;
