var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var OrderSchema = new Schema({
    orderId:{type:String, required:true, index:{unique:true}},
    orderDes:{type:String, required:true,},
    quantity:{type:Number, required:true},
    supplierId:{type:String, required:true,},
    GRN_status:{type:String, required:true},
    quality_status:{type:String, required:true},



});

const order= mongoose.model('order',OrderSchema)
module.exports = order;



