var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var SalesDetailsSchema = new Schema({
    salesDetailsId:{type:String, required:true,},
    reciptNum:{type:String, required:true,},
    invoiceId:{type:String, required:true},
    createdAt:{type:String, required:true,},
    paymentType:{type:String, required:true},
    amount:{type:String, required:true},
    updatedAt:{type:String, required:false},
    productId:{type:String, required:false},
    ProductName:{type:String, required:false},
    qty:{type:String, required:false},
    total:{type:String, required:false}


});

const SalesDetails= mongoose.model('SalesDetails',SalesDetailsSchema)
module.exports = SalesDetails;
