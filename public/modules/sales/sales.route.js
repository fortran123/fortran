const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const salesModel = mongoose.model('Sales');
const Router = express.Router();



Router.post('/', (req,res) => {
    const sales = new salesModel(req.body);
    sales.save().then(sales => {
        res.json(sales);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/', (req, res) => {
    salesModel.find().exec().then(salesdata => {
        res.json(salesdata);
        //console.log(req);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});
//sales by customername
Router.get('/:id', (req, res) => {
    salesModel.find({ "customername": req.params.id }).exec().then(salesdataspecific => {
        res.json(salesdataspecific);
        //console.log(req);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});


Router.put('/:id', (req, res) => {
    const details = req.body;
    delete details.customername;
    const customernameparam = req.params.id;
    salesModel.update({customername:customernameparam}, {$set: details}).then(salesupdate => {
        console.log(req.body);
        res.json(salesupdate);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});



module.exports = Router;
