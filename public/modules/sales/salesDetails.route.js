const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const SalesDetailsModel = mongoose.model('SalesDetails');
const Router = express.Router();



Router.post('/', (req,res) => {
    console.log("hhh");
    const salesDetails = new SalesDetailsModel(req.body);
    // console.log(salesDetails);
    salesDetails.save().then(salesDetails => {
        res.json(salesDetails);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/', (req, res) => {
    SalesDetailsModel.find().exec().then(salesDetailsdata => {
        res.json(salesDetailsdata);

    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});
//salesDetails by customername
Router.get('/:id', (req, res) => {
    SalesDetailsModel.find({ "invoiceId": req.params.id }).exec().then(salesDetailsdataspecific => {
        res.json(salesDetailsdataspecific);

    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});


Router.put('/:id', (req, res) => {
    const details = req.body;
    delete details.invoiceId;
    const invoiceIdparam = req.params.id;
    SalesDetailsModel.update({invoiceId:invoiceIdparam}, {$set: details}).then(salesDetailsupdate => {
        console.log(req.body);
        res.json(salesDetailsupdate);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});



module.exports = Router;
