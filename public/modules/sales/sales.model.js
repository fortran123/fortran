var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var SalesSchema = new Schema({
    customername:{type:String, required:true,},
    reciptNum:{type:String, required:true,},
    invoiceId:{type:String, required:true},
    createdAt:{type:String, required:true,},
    paymentType:{type:String, required:true},
    amount:{type:String, required:true},
    updatedAt:{type:String, required:false}


});

const Sales= mongoose.model('Sales',SalesSchema)
module.exports = Sales;
