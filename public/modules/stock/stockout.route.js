const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const stockOutModel = mongoose.model('StockOut');
const Router = express.Router();



Router.post('/', (req,res) => {
    const stockout = new stockOutModel(req.body);
stockout.save().then(stockout => {
    res.json(stockout);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});

Router.get('/', (req, res) => {
    stockOutModel.find().exec().then(stockoutdata => {
    res.json(stockoutdata);
    console.log(stockoutdata);
//console.log(req);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});

Router.get('/:id', (req, res) => {
    stockOutModel.find({ "ProductID": req.params.id }).exec().then(stockoutdataspecific => {
    res.json(stockoutdataspecific);
//console.log(req);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});


Router.put('/:id', (req, res) => {
    const details = req.body;
delete details.ProductID;
const ProductIDa = req.params.id;
stockOutModel.update({ProductID:ProductIDa}, {$set: details}).then(stockoutupdate => {
    res.json(stockoutupdate);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});



Router.delete('/:id', (req, res) => {
    const details = req.body;
delete details.ProductID;
const ProductIDa = req.params.id;
stockOutModel.remove({ProductID:ProductIDa}).then(stockoutdelete => {
    res.sendStatus(200);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});

module.exports = Router;


