var mongoose= require("mongoose");
const Schema = mongoose.Schema;
//autoIncrement = require('mongoose-auto-increment');

var StockInSchema = new Schema({
    ProductID:{type:String, required:true,index:{unique:true}},
    PackageID:{type:String, required:true, index:{unique:true}},
    Brand:{type:String, required:false},
    DrugName:{type:String, required:true,},
    DosageForm:{type:String, required:true},
    Quantity:{type:String, required:true},
    NationalDrugCode:{type:String, required:true},
    LotNo:{type:String, required:true},
    ExpirationDate:{type:String, required:true},
    NameOfManufacturerer:{type:String, required:true},
    StorageRequirement:{type:String, required:true}

});

const StockIn= mongoose.model('StockIn',StockInSchema)
module.exports = StockIn;
