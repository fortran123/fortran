var mongoose= require("mongoose");
const Schema = mongoose.Schema;
//autoIncrement = require('mongoose-auto-increment');

var StockOutSchema = new Schema({
    ProductID:{type:String, required:true,index:{unique:true}},
    PackageID:{type:String, required:true, index:{unique:true}},
    Brand:{type:String, required:false},
    DrugName:{type:String, required:true,},
    Quantity:{type:String, required:true},
    LotNo:{type:String, required:true},
    NameOfManufacturerer:{type:String, required:true},
    StockOutDate:{type:String, required:true},
    Reason:{type:String, required:true}

});

const StockOut= mongoose.model('StockOut',StockOutSchema)
module.exports = StockOut;
