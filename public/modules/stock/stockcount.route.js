const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const stockCountModel = mongoose.model('StockCount');
const Router = express.Router();



Router.post('/', (req,res) => {
    const stockin = new stockCountModel(req.body);
StockCount.save().then(StockCount => {
    res.json(StockCount);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});

Router.get('/', (req, res) => {
    stockCountModel.find().exec().then(stockcountdata => {
    res.json(stockcountdata);
//console.log(req);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});

Router.get('/:id', (req, res) => {
    stockCountModel.find({ "PackageID": req.params.id }).exec().then(stockcountdataspecific => {
    res.json(stockcountdataspecific);
//console.log(req);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});


Router.put('/:id', (req, res) => {
    const details = req.body;
delete details.PackageID;
const PackageIDa = req.params.id;
stockCountModel.update({PackageID:PackageIDa}, {$set: details}).then(stockcountupdate => {
    res.json(stockcountupdate);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});



Router.delete('/:id', (req, res) => {
    const details = req.body;
delete details.PackageID;
const PackageIDa = req.params.id;
stockCountModel.remove({PackageID:PackageIDa}).then(stockcountdelete => {
    res.sendStatus(200);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});

module.exports = Router;

