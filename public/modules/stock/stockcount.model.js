var mongoose= require("mongoose");
const Schema = mongoose.Schema;
//autoIncrement = require('mongoose-auto-increment');

var StockCountSchema = new Schema({
    PackageID:{type:String, required:true, index:{unique:true}},
    Brand:{type:String, required:false},
    DrugName:{type:String, required:true,},
    QuantityOfProductsOrdered:{type:String, required:true},
    RemainingQuantity:{type:String, required:true},
    LotNo:{type:String, required:true},
    ExpirationDate:{type:String, required:true},
    StockCountDate:{type:String, required:true},
    StockCountMethod:{type:String, required:true},
    NameOfManufacturerer:{type:String, required:true}

});

const StockCount= mongoose.model('StockCount',StockCountSchema)
module.exports = StockCount;

