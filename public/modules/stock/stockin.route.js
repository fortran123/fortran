const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const stockInModel = mongoose.model('StockIn');
const Router = express.Router();



Router.post('/', (req,res) => {
    const stockin = new stockInModel(req.body);
stockin.save().then(stockin => {
    res.json(stockin);
}).catch(err => {
    console.error(err);
    res.sendStatus(500);
    });
});

Router.get('/', (req, res) => {
    stockInModel.find().exec().then(stockindata => {
    res.json(stockindata);
//console.log(req);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});

Router.get('/:id', (req, res) => {
    stockInModel.find({ "ProductID": req.params.id }).exec().then(stockindataspecific => {
    res.json(stockindataspecific);
//console.log(req);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});


Router.put('/:id', (req, res) => {
    const details = req.body;
delete details.ProductID;
const ProductIDa = req.params.id;
stockInModel.update({ProductID:ProductIDa}, {$set: details}).then(stockinupdate => {
    res.json(stockinupdate);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});



Router.delete('/:id', (req, res) => {
    const details = req.body;
delete details.ProductID;
const ProductIDa = req.params.id;
stockInModel.remove({ProductID:ProductIDa}).then(stockindelete => {
    res.sendStatus(200);
}).catch(err => {
    console.error(err);
res.sendStatus(500);
});
});

module.exports = Router;

