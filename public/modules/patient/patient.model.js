var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var PatientSchema = new Schema({
    firstname:{type:String, required:true, index:{unique:true}},
    lastname:{type:String, required:true},
    middlename:{type:String, required:true},
    Date:{type:String, required:true},
    Time:{type:String, required:true}
});

const Patient= mongoose.model('Patient',UserSchema)
module.exports = Patient;
