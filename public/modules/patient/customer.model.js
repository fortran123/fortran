/**
 * Created by Loch on 6/30/2017.
 */
var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var CustomerSchema = new Schema({
    CustomerName:{type:String, required:true, },
    CustomerTelephone:{type:Number, required:true},
    CustomerSpecilization:{type:String, required:true}
});

const Customer= mongoose.model('Customer',CustomerSchema)
module.exports = Customer;