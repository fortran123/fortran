var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var StockorderSchema = new Schema({
    name:{type:String, required:true},
    category:{type:String, required:true,},
    quantity:{type:String, required:true,},
    status:{type:String, required:false}

});

const Stockorder= mongoose.model('Stockorder',StockorderSchema)
module.exports = Stockorder;