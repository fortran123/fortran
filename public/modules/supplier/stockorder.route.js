const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const stockorder = mongoose.model('Stockorder');
const Router = express.Router();



Router.post('/', (req,res) => {
    const supplier = new stockorder(req.body);
    supplier.save().then(supplier => {
        res.json(supplier);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/', (req, res) => {
    stockorder.find().exec().then(supplierdata => {
        res.json(supplierdata);
        //console.log(req);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});


Router.put('/:id', (req, res) => {
    const details = req.body;
    delete details._id;
    const companynameupdateid = req.params.id;
    stockorder.update({_id:companynameupdateid}, {$set: details}).then(supplierupdate => {
        res.json(supplierupdate);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;
