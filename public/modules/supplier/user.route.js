const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const userModel = mongoose.model('User');
const Router = express.Router();



Router.post('/', (req,res) => {
    const user = new userModel(req.body);
    user.save().then(user => {
        res.json(user);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/', (req, res) => {
    userModel.find().exec().then(userdata => {
        res.json(userdata);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});


Router.get('/:id', (req, res) => {
    userModel.find({ "cus_ID": req.params.id }).exec().then(specificuserdata => {
        res.json(specificuserdata);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});
module.exports = Router;
