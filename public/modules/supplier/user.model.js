
var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var UserSchema = new Schema({
    cus_ID:{type:String, required:true,index:{unique:true}},
    firstname:{type:String, required:true,},
    lastname:{type:String, required:true},
    //middlename:{type:String, required:true},

    address:{type:String, required:true},
    date:{type:String, required:true},
    time:{type:String, required:true},

    patient_type:{type:String, required:true},
    diagnosis:{type:String, required:true},
    contactno:{type:Number, required:true}


});

const User= mongoose.model('User',UserSchema)
module.exports = User;