const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const supplierModel = mongoose.model('Supplier');
const Router = express.Router();



Router.post('/', (req,res) => {
    const supplier = new supplierModel(req.body);
    supplier.save().then(supplier => {
        res.json(supplier);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/', (req, res) => {
    supplierModel.find().exec().then(supplierdata => {
        res.json(supplierdata);
        //console.log(req);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/:id', (req, res) => {
    supplierModel.find({ "companyname": req.params.id }).exec().then(supplierdataspecific => {
        res.json(supplierdataspecific);
        //console.log(req);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});


Router.put('/:id', (req, res) => {
    const details = req.body;
    delete details._id;
    const companynameupdateid = req.params.id;
    supplierModel.update({_id:companynameupdateid}, {$set: details}).then(supplierupdate => {
        res.json(supplierupdate);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});



Router.delete('/:id', (req, res) => {
    const details = req.body;
    delete details.companyname;
    const companynameaid = req.params.id;
    supplierModel.remove({_id:companynameaid}).then(supplierdelete => {
        res.sendStatus(200);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;
