var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var SupplyorderSchema = new Schema({
    ordernumber:{type:String, required:true},
    selectedName:{type:String, required:true,},
    description:{type:String, required:false},
    createddate:{type:String, required:true,},
    grnstatus:{type:String, required:false},
    qualitystatus:{type:String, required:true,}

});

const Supplyorder= mongoose.model('Supplyorder',SupplyorderSchema)
module.exports = Supplyorder;