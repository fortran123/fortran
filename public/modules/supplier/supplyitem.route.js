const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const supplyitemModel = mongoose.model('Supplyitem');
const supplyorderModel = mongoose.model('Supplyorder');
const Router = express.Router();

Router.post('/', (req,res) => {
    const supplier = new supplyorderModel(req.body);
    supplier.save().then(supplier => {
        res.json(supplier);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.post('/item', (req,res) => {
    const supplieritem = new supplyitemModel(req.body);
    supplieritem.save().then(supplieritem => {
        res.json(supplieritem);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/:id', (req, res) => {
    supplyorderModel.find({ "ordernumber": req.params.id }).exec().then(supplieritemdata => {
        res.json(supplieritemdata);
        //console.log(req.params.id);
        //console.log(supplieritemdata);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/item/:id', (req, res) => {
    supplyitemModel.find({ "ordernumberitem": req.params.id }).exec().then(supplieritemdata => {
        res.json(supplieritemdata);
        //console.log(req.params.id);
        //console.log(supplieritemdata);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.delete('/item/:id', (req, res) => {
    const suppitemid = req.params.id;
    supplyitemModel.remove({_id:suppitemid}).then(supplierdelete => {
        res.sendStatus(200);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});


module.exports = Router;
