var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var SuplierSchema = new Schema({
    companyname:{type:String, required:true,index:{unique:true}},
    agentname:{type:String, required:true,},
    category:{type:String, required:true,},
    address:{type:String, required:false},
    createddate:{type:String, required:true,},
    period:{type:String, required:true},
    contactno:{type:String, required:true}

});

const Supplier= mongoose.model('Supplier',SuplierSchema)
module.exports = Supplier;