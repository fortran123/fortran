
const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);
const suppliermModel = mongoose.model('Supplier');
const supplyitemModel = mongoose.model('Supplyitem');
const supplyorderModel = mongoose.model('Supplyorder');
const Router = express.Router();

Router.get('/', (req, res) => {
    supplyorderModel.find({ "grnstatus": "created" }).exec().then(supplieritemdata => {
        res.json(supplieritemdata);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});


Router.get('/:id', (req, res) => {
    supplyitemModel.find({ "ordernumberitem": req.params.id , "quality":"accepted" }).exec().then(supplieritemdata => {
        res.json(supplieritemdata);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;