var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var SupplyitemSchema = new Schema({
    ordernumberitem:{type:String, required:true},
    orderdetailnumber:{type:String, required:true,},
    itemname:{type:String, required:false},
    itemquantity:{type:String, required:true,},
    itemprice:{type:String, required:true},
    expiredate:{type:String, required:true},
    brand:{type:String, required:true},
    status:{type:String, required:true},
    quality:{type:String, required:true}

});

const Supplyitem= mongoose.model('Supplyitem',SupplyitemSchema)
module.exports = Supplyitem;