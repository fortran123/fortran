/**
 * Created by SandaHira on 6/29/2017.
 */
const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);
const suppliermModel = mongoose.model('Supplier');
const supplyitemModel = mongoose.model('Supplyitem');
const supplyorderModel = mongoose.model('Supplyorder');
const goodrecieveModel = mongoose.model('Goodrecieve');
const Router = express.Router();


Router.get('/', (req, res) => {
    supplyorderModel.find({grnstatus:"not received",qualitystatus:"not checked"}).exec().then(supplieritemdatao => {
        res.json(supplieritemdatao);
        console.log('fdsfds');
        console.log(supplieritemdatao);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/all_po', (req, res) => {
    supplyorderModel.find().exec().then(supplieritemdatao => {
        res.json(supplieritemdatao);
        console.log('fdsfdsw888');
        console.log(supplieritemdatao);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/item/:id', (req, res) => {
    supplyitemModel.find({ "ordernumberitem": req.params.id }).exec().then(supplieritemdata => {
        res.json(supplieritemdata);
        console.log(req.params.id);
        console.log(supplieritemdata);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });

});

Router.get('/:po_ID/item/:id', (req, res) => {
    supplyitemModel.find({ "ordernumberitem": req.params.id,"orderdetailnumber":req.params.po_ID }).exec().then(itemDetailsForPecificPo => {
        res.json(itemDetailsForPecificPo);
        console.log(req.params.po_ID);
        console.log(itemDetailsForPecificPo);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });

});

Router.put('/:po_ID/item/:id', (req, res) => {
    const details = req.body;
    delete details._id;
    console.log(details);
    const companynameupdateid = req.params.id;
    supplyitemModel.update({"ordernumberitem": req.params.id,"orderdetailnumber":req.params.po_ID}, {"quality": details.quality,"status":details.status}).then(supplierupdate => {
        res.json(supplierupdate);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

//get selected supplier details
Router.get('/supplier/:id', (req,res) => {

    supplierModel.find({"companyname": req.params.id}).exec().then(supplier => {
        res.json(supplier);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});
//agent for given order
Router.get('/supplier/:id/agent', (req, res) => {
    supplyorderModel.find({ "ordernumber": req.params.id }).exec().then(supplieritemdata => {
        res.json(supplieritemdata);
        console.log(req.params.id);
        console.log(supplieritemdata);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});
Router.put('/:id', (req, res) => {
    const details = req.body;
    delete details._id;
    console.log(details);
    const companynameupdateid = req.params.id;
    supplyorderModel.update({"ordernumber": req.params.id}, {"grnstatus": details.grnstatus}).then(supplierupdate => {
        res.json(supplierupdate);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/rejected', (req, res) => {
    supplyitemModel.find({ "quality": "rejected" }).exec().then(supplieritemdatarej => {
        res.json(supplieritemdatarej);
        console.log(supplieritemdatarej);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });

});


Router.post('/goodrecieve', (req,res) => {
    const gooditem = new goodrecieveModel(req.body);
    gooditem.save().then(greitem => {
        res.json(greitem);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.get('/goodrecieve/:id/qty/:po_ID', (req,res) => {
    goodrecieveModel.find({ "ordernumberitem": req.params.id,"orderdetailnumber":req.params.po_ID }).
    exec().then(grnitem => {
        res.json(grnitem);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;
