var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var goodreceiveSchema = new Schema({
    ordernumberitem:{type:String, required:true},
    orderdetailnumber:{type:String, required:true,},
    itemname:{type:String, required:false},
    itemquantity:{type:String, required:true,},
    expiredate:{type:String, required:true},
    brand:{type:String, required:true}

});

const goodreceive= mongoose.model('Goodrecieve',goodreceiveSchema)
module.exports = goodreceive;