var mongoose= require("mongoose");
const Schema = mongoose.Schema;

var CommonSchema = new Schema({
    name:{type:String, required:true,},
    message:{type:String, required:true,},
    status:{type:String, required:true}
});

const Common= mongoose.model('notification',CommonSchema)
module.exports = Common;