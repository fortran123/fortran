const express = require('express');
const mongoose= require("mongoose");

const Schema = mongoose.Schema;

mongoose.set('debug', false);

const commonModel = mongoose.model('notification');
const Router = express.Router();


Router.get('/', (req, res) => {
    commonModel.find({'name': 'sanda','status':'false'}).exec().then(commondata => {
        res.json(commondata);;
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});
Router.put('/', (req, res) => {
    const driver = req.body;
    delete driver._id;
    const notifyId = driver.id;
    commonModel.findByIdAndUpdate(notifyId, {'status':'true'}).then(notifyDb => {
        res.json(notifyDb);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

Router.post('/', (req,res) => {
    const insertnotify = new commonModel(req.body);
    insertnotify.save().then(inotify => {
        res.json(inotify);
        console.log(inotify);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;
