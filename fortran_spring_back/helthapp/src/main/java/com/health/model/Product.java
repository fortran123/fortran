package com.health.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

/**
 * Created by User on 6/28/2017.
 */
public class Product {

    @Id
    private  String productId;

    @NotEmpty
    private String productName;
    @NotEmpty
    private String unitAmount;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String producttID) {
        this.productId = producttID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUnitAmount() {
        return unitAmount;
    }

    public void setUnitAmount(String unitAmount) {
        this.unitAmount = unitAmount;
    }
}
