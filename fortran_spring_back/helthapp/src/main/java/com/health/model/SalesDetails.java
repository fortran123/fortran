package com.health.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;

/**
 * Created by User on 6/30/2017.
 */
public class SalesDetails {

    @Id
    private  String salesDetailsId;
    @NotEmpty
    private  String invoiceId;

    @NotEmpty
    private String createdAt;

    @NotEmpty
    private String productName;
    @NotEmpty
    private String qty;

    @NotEmpty
    private String unitAmount;

    @NotEmpty
    private String total;

    public String getSalesDetailsId() {
        return salesDetailsId;
    }

    public void setSalesDetailsId(String salesDetailsId) {
        this.salesDetailsId = salesDetailsId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUnitAmount() {
        return unitAmount;
    }

    public void setUnitAmount(String unitAmount) {
        this.unitAmount = unitAmount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
