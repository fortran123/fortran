package com.health.controller;

import com.health.model.Login;
import com.health.model.Payment;
import com.health.model.Product;
import com.health.service.loginService;
import com.health.service.productService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by User on 6/21/2017.
 */
@RestController
public class loginController {


@Autowired
private loginService loginservice;

    @CrossOrigin
    @RequestMapping(value = "/login", method = RequestMethod.POST ,consumes ="application/json")
    public Login createLogin(@Validated @RequestBody Login login){
    return loginservice.Createlogin(login);

}

    @CrossOrigin
    @RequestMapping(value = "/login", method = RequestMethod.GET ,produces ="application/json")
    List<Login> getAllLogin(){
        return loginservice.getAllLogin();

    }

    @CrossOrigin
    @RequestMapping(value = "/payment/{username}/{password}", method = RequestMethod.GET ,produces ="application/json")
    public Login findUserByUsernameandPassword(@PathVariable("username") String username,@PathVariable("password") String password){
        return loginservice.findUserbyUsernameAndPassword(username, password);

    }









}
