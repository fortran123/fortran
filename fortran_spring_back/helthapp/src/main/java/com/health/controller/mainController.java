package com.university.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by User on 6/21/2017.
 */
@Controller
public class mainController {

    @RequestMapping(value="/",method = RequestMethod.GET)
    public String homepage(){
        return "index.html";
    }

    @RequestMapping(value="/mystudent",method = RequestMethod.GET)
    public String student(){
        return "studentView.html";
    }
}
