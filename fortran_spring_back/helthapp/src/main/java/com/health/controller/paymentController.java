package com.health.controller;

import com.health.model.Payment;
import com.health.service.paymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by User on 6/21/2017.
 */
@RestController
public class paymentController {


@Autowired
private paymentService paymentservice;

    @CrossOrigin
    @RequestMapping(value = "/payment", method = RequestMethod.POST ,consumes ="application/json")
    public Payment createPayment(@Validated @RequestBody Payment payment){
    return paymentservice.CreatePayment(payment);

}

    @CrossOrigin
    @RequestMapping(value = "/payment", method = RequestMethod.GET ,produces ="application/json")
    List<Payment> getAllPayments(){
        return paymentservice.getAllPayments();

    }

    @CrossOrigin
    @RequestMapping(value = "/payment/{invoiceId}", method = RequestMethod.GET ,produces ="application/json")
    public List<Payment> getPaymentByinvoiceId( @PathVariable("invoiceId") String invoiceId){
        return paymentservice.getPaymentByinvoiceId(invoiceId);

    }

    @CrossOrigin
    @RequestMapping(value = "/getcountpayment", method = RequestMethod.GET ,produces ="application/json")
    public Long getPaymentDocCount( ){
        return paymentservice.getLastInvoiceId();

    }


    @CrossOrigin
    @RequestMapping(value = "/paymentcustomer/{customerId}", method = RequestMethod.GET ,produces ="application/json")
    public List<Payment> getPaymentBycustomerId( @PathVariable("customerId") String customerId){
        return paymentservice.getPaymentBycustomerId(customerId);

    }

    @CrossOrigin
    @RequestMapping(value = "/paymenttype/{type}", method = RequestMethod.GET ,produces ="application/json")
    public List<Payment> getPaymentBytype( @PathVariable("type") String type){
        return paymentservice.getPaymentBytype(type);

    }


}
