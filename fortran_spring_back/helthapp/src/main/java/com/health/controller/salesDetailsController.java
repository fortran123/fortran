package com.health.controller;

import com.health.model.Payment;
import com.health.model.SalesDetails;
import com.health.service.paymentService;
import com.health.service.salesDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by User on 6/21/2017.
 */
@RestController
public class salesDetailsController {


@Autowired
private salesDetailsService salesDetailsservice;

    @CrossOrigin
    @RequestMapping(value = "/salesDetails", method = RequestMethod.POST ,consumes ="application/json")
    public SalesDetails CreateSalesDetails(@Validated @RequestBody SalesDetails salesDetails){
    return salesDetailsservice.CreateSalesDetails(salesDetails);

}

    @CrossOrigin
    @RequestMapping(value = "/salesDetails", method = RequestMethod.GET ,produces ="application/json")
    List<SalesDetails> getAllSalesDetails(){
        return salesDetailsservice.getAllSalesDetails();

    }

    @CrossOrigin
    @RequestMapping(value = "/salesDetails/{invoiceId}", method = RequestMethod.GET ,produces ="application/json")
    public List<SalesDetails> getSelectedSalesDetail( @PathVariable("invoiceId") String invoiceId){
        return salesDetailsservice.getSelectedSalesDetail(invoiceId);

    }






}
