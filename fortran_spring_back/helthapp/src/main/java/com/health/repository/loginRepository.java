package com.health.repository;

import com.health.model.Login;
import com.health.model.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by User on 6/21/2017.
 */
@Repository
public interface loginRepository extends MongoRepository<Login,String>{

    Login findByUsernameAndPassword(String username, String password);


}
