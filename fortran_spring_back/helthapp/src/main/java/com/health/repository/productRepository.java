package com.health.repository;

import com.health.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by User on 6/28/2017.
 */
@Repository
public interface productRepository extends MongoRepository<Product,String> {
}
