package com.health.repository;

import com.health.model.SalesDetails;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by User on 6/30/2017.
 */
@Repository
public interface salesDetailsRepository extends MongoRepository<SalesDetails,String>{

    List<SalesDetails> findByinvoiceId(String invoiceId);





}
