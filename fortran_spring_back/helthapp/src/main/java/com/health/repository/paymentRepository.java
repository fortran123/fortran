package com.health.repository;

import com.health.model.Payment;
import com.health.model.SalesDetails;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by User on 6/21/2017.
 */
@Repository
public interface paymentRepository extends MongoRepository<Payment,String>{

    List<Payment> findByinvoiceId(String invoiceId);
    List<Payment> findBycustomerId(String customerId);
    List<Payment> findBytype(String type);

}
