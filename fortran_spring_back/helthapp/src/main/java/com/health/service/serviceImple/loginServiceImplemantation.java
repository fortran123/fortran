package com.health.service.serviceImple;

import com.health.model.Login;
import com.health.repository.loginRepository;
import com.health.service.loginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by User on 6/21/2017.
 */
@Service
public class loginServiceImplemantation implements loginService {

@Autowired
private loginRepository loginRepo;


    @java.lang.Override
    public Login Createlogin(Login login) {
        login.setLoginID(UUID.randomUUID().toString());
        return loginRepo.save(login);
    }

    @java.lang.Override
    public List<Login> getAllLogin() {
        return loginRepo.findAll();
    }

    @java.lang.Override
    public Login findUserbyUsernameAndPassword(String username, String password) {
        return loginRepo.findByUsernameAndPassword(username,password);
    }
}
