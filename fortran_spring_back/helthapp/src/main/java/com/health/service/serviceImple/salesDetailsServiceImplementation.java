package com.health.service.serviceImple;

import com.health.model.SalesDetails;
import com.health.repository.salesDetailsRepository;
import com.health.service.salesDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by User on 6/30/2017.
 */
@Service
public class salesDetailsServiceImplementation implements salesDetailsService {

    @Autowired
    salesDetailsRepository salesDetailRepo;


    @Override
    public SalesDetails CreateSalesDetails(SalesDetails salesDetails) {
        salesDetails.setSalesDetailsId(UUID.randomUUID().toString());
        return salesDetailRepo.save(salesDetails);
    }

    @Override
    public List<SalesDetails> getAllSalesDetails() {
        return salesDetailRepo.findAll();
    }

    @Override
    public List<SalesDetails>  getSelectedSalesDetail(String invoiceId) {
        return salesDetailRepo.findByinvoiceId(invoiceId);
    }
}
