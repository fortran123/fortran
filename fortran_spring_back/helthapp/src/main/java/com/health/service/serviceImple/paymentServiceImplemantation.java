package com.health.service.serviceImple;

import com.health.model.Payment;
import com.health.repository.paymentRepository;
import com.health.service.paymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by User on 6/21/2017.
 */
@Service
public class paymentServiceImplemantation implements paymentService {

@Autowired
private paymentRepository paymentRepo;


    @Override
    public Payment CreatePayment(Payment payment) {
        payment.setPaymentID(UUID.randomUUID().toString());
        return paymentRepo.save(payment);
    }

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepo.findAll();
    }

    @Override
    public List<Payment> getPaymentByinvoiceId(String invoiceId) {
        return paymentRepo.findByinvoiceId(invoiceId);
    }

    @Override
    public Long getLastInvoiceId() {
        return paymentRepo.count();
    }

    @Override
    public List<Payment> getPaymentBycustomerId(String customerId) {
        return paymentRepo.findBycustomerId(customerId);
    }

    @Override
    public List<Payment> getPaymentBytype(String type) {
        return paymentRepo.findBytype(type);
    }
}
