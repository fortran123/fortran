package com.health.service.serviceImple;

import com.health.model.Payment;
import com.health.model.Product;
import com.health.repository.paymentRepository;
import com.health.repository.productRepository;
import com.health.service.paymentService;
import com.health.service.productService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by User on 6/21/2017.
 */
@Service
public class productServiceImplemantation implements productService {

@Autowired
private productRepository productRepo;


    @Override
    public Product CreateProduct(Product product) {
        product.setProductId(UUID.randomUUID().toString());
        return productRepo.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepo.findAll();
    }

}
