package com.health.service;

import com.health.model.SalesDetails;

import java.util.List;

/**
 * Created by User on 6/30/2017.
 */
public interface salesDetailsService {

    SalesDetails CreateSalesDetails(SalesDetails salesDetails);
    List<SalesDetails> getAllSalesDetails();
    List<SalesDetails>  getSelectedSalesDetail(String invoiceId);

}
