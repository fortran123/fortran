package com.health.service;

import com.health.model.Payment;

import java.util.List;

/**
 * Created by User on 6/21/2017.
 */
public interface paymentService {

    Payment CreatePayment(Payment payment);
    List<Payment> getAllPayments();
    List<Payment> getPaymentByinvoiceId(String invoiceId);
    Long getLastInvoiceId();
    List<Payment> getPaymentBycustomerId(String customerId);
    List<Payment> getPaymentBytype(String type);




}
